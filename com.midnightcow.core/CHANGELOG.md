# Changelog
All notable changes to this package will be documented in this file. The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [1.0.0] - 2023-10-14
- Package created

## [1.0.1] - 2023-10-14
- Updated readme and SystemHelpers examples text

## [1.1.0] - 2023-10-14
- Added PhysicsHelpers ( depends on presence of Unity.Physics assembly )
- Changed namespaces

## [1.2.0] - 2023-10-14
- Added git URL instructions to README
- Added version defines to asdef
- Added error-message if project does not contain entities packages ( no forced dependency )

## [1.2.1] - 2023-10-17
- Fixed blatantly obvious problem with SystemHelpers.GetEntityComponentArraysAsyncJob not filling arrays with a proper index, which essentially meant it didn't work at all.
- GetEntityComponentArraysAsyncJob refactored to allow ienableable components.

## [1.3.0] - 2023-10-18
- Added some ClearBuffers/ClearLists helper jobs to SystemHelpers
- Added 'scheduleParallel' option to the entities/components collection helpers
- Single threaded option just uses an int to index the entities for writing to arrays
- Parallel option schedules CalculateBaseEntityIndexArrayAsync to collect an index array first
