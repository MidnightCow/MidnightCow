﻿# MidnightCow Core

Some helpers and stuff for Unity and Unity's ECS/DOTS ecosystem.

This is a very serious library built for Unity's cutting-edge data oriented technology stack - and for that reason, here's a picture of a seagull:

![enter image description here](https://gitlab.com/MidnightCow/MidnightCow/-/raw/main/img_seagull_noreason.png)

