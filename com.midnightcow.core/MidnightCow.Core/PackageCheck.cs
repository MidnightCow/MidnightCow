﻿
namespace MidnightCow.Core
{
    // NOT FORCING DEPENDENCIES BECAUSE IT's ANNOYING - JUST THROW AN ERROR //
#if !(UNITY_ENTITIES_EXISTS && UNITY_COLLECTIONS_EXISTS && UNITY_BURST_EXISTS)
#error MidnightCow Core requires Unity.Entities/Burst/Collections in order to function.
#endif
}
