using Unity.Jobs;
using MidnightCow.Core.Systems.Helpers;

// assembly: MIDNIGHTCOW.CORE //

[assembly: RegisterGenericJobType(typeof(SystemHelpers.GetEntityComponentArraysAsyncJob<SystemHelpers.dummytype, SystemHelpers.dummytype>))]
[assembly: RegisterGenericJobType(typeof(SystemHelpers.GetEntityComponentArraysAsyncParallelJob<SystemHelpers.dummytype, SystemHelpers.dummytype>))]