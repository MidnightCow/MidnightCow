﻿
namespace MidnightCow.Core.Physics.Helpers
{
#if UNITY_PHYSICS_EXISTS

    using Unity.Collections;
    using Unity.Physics;

    /// <summary> Useful functions when using Unity.Physics. </summary>
    public static class PhysicsHelpers
    {
        /// <summary> Check if physics body has tag (as defined in custom PhysicsBody->Advanced tab and stored in PhysicsCustomTags component)</summary>
        /// <param name="bodies"> PhysicsWorldSingleton.bodies </param>
        /// <param name="bodyIndex"> Often retrieved in collision events (statefulEvent.BodyIndexA). </param>
        /// <param name="tagToCheck"> Tag to check for. </param>
        /// <returns> True if body has tag. </returns>
        public static bool HasBodyTag(in NativeArray<RigidBody> bodies, int bodyIndex, byte tagToCheck) => (bodies[bodyIndex].CustomTags & tagToCheck) != 0;
    }

#endif
}
