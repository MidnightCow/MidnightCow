﻿
namespace MidnightCow.Core.Systems.Helpers
{
    using Unity.Burst;
    using Unity.Collections;
    using Unity.Entities;
    using Unity.Jobs;
    using Unity.Assertions;
    using Unity.Burst.Intrinsics;
    using Unity.Collections.LowLevel.Unsafe;
    using Unity.Entities.UniversalDelegates;
    using System;

    /// <summary> Useful functions when working inside ISystems. </summary>
    public static class SystemHelpers
    {
        /*
        
        [IMPORTANT]

        When you use any of the generic ( requiring T type ) IJobChunk based methods, you'll have to Register the specific jobs 
        with types that are used. Easiest way is to create an empty cs file somewhere in the project or assembly root, and do it there.
        
        Using the LocalTransform example from below as a reference register the job like:
        
            [assembly: RegisterGenericJobType(typeof(SystemHelpers.GetEntityComponentArraysAsyncJob<LocalTransform, SystemHelpers.dummytype>))]
        
        Or in the case of getting two component arrays like:

            [assembly: RegisterGenericJobType(typeof(SystemHelpers.GetEntityComponentArraysAsyncJob<LocalTransform, PhysicsVelocity>))]
        
        And when using the `scheduleParallel` option like:

            [assembly: RegisterGenericJobType(typeof(SystemHelpers.GetEntityComponentArraysAsyncParallelJob<LocalTransform, PhysicsVelocity>))]

        Same goes for the ClearBuffersJob ( IJobChunk ):
                    
            [assembly: RegisterGenericJobType(typeof(SystemHelpers.ClearBuffersJob<SquadUnit>))]

        When only retrieving Entities it's not required. The SystemHelpers.dummytype is just used as mentioned below as a
        null/placeholder when not using that second type.
        
        */

        /*
        
        JUST SOME HELPER METHODs to allow scheduling jobs to clear buffers and lists.
        Doing it in a job keeps it off the main thread and helps to ensure timing by
        scheduling these between your other jobs.

        I was doing this a lot so made sense to create some readymade generic jobs for it.

        ClearListJob - pass in the rw list you want to clear
        ClearBufferJob - pass in the rw buffer you want to clear
        ClearBuffersJob - an IJobChunk which will iterate and clear all buffers of type.


        */

        [BurstCompile]
        public partial struct ClearListJob<T> : IJob where T : unmanaged
        {
            public NativeList<T> list;
            public void Execute()
            {
                if (list.IsCreated) list.Clear();
            }
        }
        [BurstCompile]
        public partial struct ClearBufferJob<T> : IJob where T : unmanaged, IBufferElementData
        {
            public DynamicBuffer<T> buffer;
            public void Execute()
            {
                if (buffer.IsCreated) buffer.Clear();
            }
        }
        [BurstCompile]
        public partial struct ClearBuffersJob<T> : IJobChunk where T : unmanaged, IBufferElementData
        {
            public BufferTypeHandle<T> TypeHandle1;
            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            {
                var bufferArray = chunk.GetBufferAccessor<T>(ref TypeHandle1);
                var enumerator = new ChunkEntityEnumerator(useEnabledMask, chunkEnabledMask, chunk.Count);
                while (enumerator.NextEntityIndex(out var i)) bufferArray[i].Clear();
            }
        }

        /*
        
        HELPER METHODS for retrieving entities and components arrays from query asynchronously.

        * Added `scheduleParallel` option - use if you've a lot of entities to collect from
        
        EntityQuery async methods throw safety errors under certain conditions, so built these.
        EntityQuery async methods are also called separately, so if you wanted to get an entities array and a component array,
        it would result in two separate jobs, this combines these separate calls into a single job.

        Job disables container and handle safety to allow schedule without creating and passing in the un-needed types.

        Tried these as extensions of SystemState but that also threw the safety errors, so passing in 'ref state' works instead.
        Uses 'dummytype' ICD type just to spoof the job constructor when not using all types.

        - USAGE EXAMPLE 1 ( getting just entity array ):
            
            var squadsQuery = QueryBuilder().WithAll<SquadData>().Build();
            SystemHelpers.GetEntityComponentArraysAsync(ref state, squadsQuery, GetEntityTypeHandle(), out var squadEntities);

        - USAGE EXAMPLE 2 ( getting entity array and LocalTransform array ):
            
            var markersQuery = QueryBuilder().WithAll<Unit_Marker_Tag, LocalTransform>().Build();
            SystemHelpers.GetEntityComponentArraysAsync(ref state, markersQuery, GetEntityTypeHandle(), out var markerEntities, GetComponentTypeHandle<LocalTransform>(), out var markerTransforms);
        
         */

        public struct dummytype : IComponentData {}

        /////////////////////////////////////////////////////// ENTITIES ARRAY ONLY //////
        public static void GetEntityComponentArraysAsync(
            bool scheduleParallel, ref SystemState state, EntityQuery query,
            EntityTypeHandle entityTypeHandle, out NativeArray<Entity> entitiesArray)
        {
            entitiesArray = CollectionHelper.CreateNativeArray<Entity>(query.CalculateEntityCount(), state.WorldUpdateAllocator);
            if (scheduleParallel)
            {
                var indexArray = query.CalculateBaseEntityIndexArrayAsync(state.WorldUpdateAllocator, state.Dependency, out var indexJobHandle);
                state.Dependency = new GetEntityComponentArraysAsyncParallelJob<dummytype, dummytype>
                {
                    ChunkBaseEntityIndices = indexArray,
                    entityType = entityTypeHandle,
                    arrayEntities = entitiesArray
                }.ScheduleParallel(query, indexJobHandle);
            }
            else
            {
                state.Dependency = new GetEntityComponentArraysAsyncJob<dummytype, dummytype>
                {
                    entityType = entityTypeHandle,
                    arrayEntities = entitiesArray
                }.Schedule(query, state.Dependency);
            }
        }
        ////////////////////////////////////////////// ENTITIES + 1 COMPONENT ARRAY //////
        public static void GetEntityComponentArraysAsync<T1>(
            bool scheduleParallel, ref SystemState state, EntityQuery query,
            EntityTypeHandle entityTypeHandle, out NativeArray<Entity> entitiesArray,
            ComponentTypeHandle<T1> componentType1Handle, out NativeArray<T1> type1Array)
            where T1 : unmanaged, IComponentData
        {
            var entityCount = query.CalculateEntityCount();
            entitiesArray = CollectionHelper.CreateNativeArray<Entity>(entityCount, state.WorldUpdateAllocator);
            type1Array = CollectionHelper.CreateNativeArray<T1>(entityCount, state.WorldUpdateAllocator);
            if (scheduleParallel)
            {
                var indexArray = query.CalculateBaseEntityIndexArrayAsync(state.WorldUpdateAllocator, state.Dependency, out var indexJobHandle);
                state.Dependency = new GetEntityComponentArraysAsyncParallelJob<T1, dummytype>
                {
                    ChunkBaseEntityIndices = indexArray,
                    entityType = entityTypeHandle,
                    arrayEntities = entitiesArray,
                    TypeHandle1 = componentType1Handle,
                    arrayType1 = type1Array
                }.ScheduleParallel(query, indexJobHandle);
            }
            else
            {
                state.Dependency = new GetEntityComponentArraysAsyncJob<T1, dummytype>
                {
                    entityType = entityTypeHandle,
                    arrayEntities = entitiesArray,
                    TypeHandle1 = componentType1Handle,
                    arrayType1 = type1Array
                }.Schedule(query, state.Dependency);
            }
        }
        ///////////////////////////////////////////// ENTITIES + 2 COMPONENT ARRAYS //////
        public static void GetEntityComponentArraysAsync<T1, T2>(
            bool scheduleParallel, ref SystemState state, EntityQuery query,
            EntityTypeHandle entityTypeHandle, out NativeArray<Entity> entitiesArray,
            ComponentTypeHandle<T1> componentType1Handle, out NativeArray<T1> type1Array,
            ComponentTypeHandle<T2> componentType2Handle, out NativeArray<T2> type2Array)
            where T1 : unmanaged, IComponentData where T2 : unmanaged, IComponentData
        {
            var entityCount = query.CalculateEntityCount();
            entitiesArray = CollectionHelper.CreateNativeArray<Entity>(entityCount, state.WorldUpdateAllocator);
            type1Array = CollectionHelper.CreateNativeArray<T1>(entityCount, state.WorldUpdateAllocator);
            type2Array = CollectionHelper.CreateNativeArray<T2>(entityCount, state.WorldUpdateAllocator);
            if (scheduleParallel)
            {
                var indexArray = query.CalculateBaseEntityIndexArrayAsync(state.WorldUpdateAllocator, state.Dependency, out var indexJobHandle); 
                state.Dependency = new GetEntityComponentArraysAsyncParallelJob<T1, T2>
                {
                    ChunkBaseEntityIndices = indexArray,
                    entityType = entityTypeHandle,
                    arrayEntities = entitiesArray,
                    TypeHandle1 = componentType1Handle,
                    arrayType1 = type1Array,
                    TypeHandle2 = componentType2Handle,
                    arrayType2 = type2Array
                }.ScheduleParallel(query, indexJobHandle);
            }
            else
            {
                state.Dependency = new GetEntityComponentArraysAsyncJob<T1, T2>
                {
                    entityType = entityTypeHandle,
                    arrayEntities = entitiesArray,
                    TypeHandle1 = componentType1Handle,
                    arrayType1 = type1Array,
                    TypeHandle2 = componentType2Handle,
                    arrayType2 = type2Array
                }.Schedule(query, state.Dependency);
            }
        }
        ////////////////////////////////////// NO ENTITIES - 1 COMPONENT ARRAY ONLY //////
        public static void GetEntityComponentArraysAsync<T1>(
            bool scheduleParallel, ref SystemState state, EntityQuery query,
            ComponentTypeHandle<T1> componentType1Handle, out NativeArray<T1> type1Array)
            where T1 : unmanaged, IComponentData
        {
            var entityCount = query.CalculateEntityCount();
            type1Array = CollectionHelper.CreateNativeArray<T1>(entityCount, state.WorldUpdateAllocator);
            if (scheduleParallel)
            {
                var indexArray = query.CalculateBaseEntityIndexArrayAsync(state.WorldUpdateAllocator, state.Dependency, out var indexJobHandle);
                state.Dependency = new GetEntityComponentArraysAsyncParallelJob<T1, dummytype>
                {
                    ChunkBaseEntityIndices = indexArray,
                    TypeHandle1 = componentType1Handle,
                    arrayType1 = type1Array
                }.ScheduleParallel(query, indexJobHandle);
            }
            else
            {
                state.Dependency = new GetEntityComponentArraysAsyncJob<T1, dummytype>
                {
                    TypeHandle1 = componentType1Handle,
                    arrayType1 = type1Array
                }.Schedule(query, state.Dependency);
            }
        }
        ////////////////////////////////////////// NO ENTITIES - 2 COMPONENT ARRAYS //////
        public static void GetEntityComponentArraysAsync<T1, T2>(
            bool scheduleParallel, ref SystemState state, EntityQuery query,
            ComponentTypeHandle<T1> componentType1Handle, out NativeArray<T1> type1Array,
            ComponentTypeHandle<T2> componentType2Handle, out NativeArray<T2> type2Array)
            where T1 : unmanaged, IComponentData where T2 : unmanaged, IComponentData
        {
            var entityCount = query.CalculateEntityCount();
            type1Array = CollectionHelper.CreateNativeArray<T1>(entityCount, state.WorldUpdateAllocator);
            type2Array = CollectionHelper.CreateNativeArray<T2>(entityCount, state.WorldUpdateAllocator);
            if (scheduleParallel)
            {
                var indexArray = query.CalculateBaseEntityIndexArrayAsync(state.WorldUpdateAllocator, state.Dependency, out var indexJobHandle);
                state.Dependency = new GetEntityComponentArraysAsyncParallelJob<T1, T2>
                {
                    ChunkBaseEntityIndices = indexArray,
                    TypeHandle1 = componentType1Handle,
                    arrayType1 = type1Array,
                    TypeHandle2 = componentType2Handle,
                    arrayType2 = type2Array
                }.ScheduleParallel(query, indexJobHandle);
            }
            else
            {
                state.Dependency = new GetEntityComponentArraysAsyncJob<T1, T2>
                {
                    TypeHandle1 = componentType1Handle,
                    arrayType1 = type1Array,
                    TypeHandle2 = componentType2Handle,
                    arrayType2 = type2Array
                }.Schedule(query, state.Dependency);
            }
        }

        /////////////////////////////////////////////////////// THE JOBS THEMSELVES //////        
        [BurstCompile]
        public partial struct GetEntityComponentArraysAsyncJob<T1, T2> : IJobChunk where T1 : unmanaged, IComponentData where T2 : unmanaged, IComponentData
        {
            [NativeDisableContainerSafetyRestriction] public EntityTypeHandle entityType;
            [NativeDisableContainerSafetyRestriction] public ComponentTypeHandle<T1> TypeHandle1;
            [NativeDisableContainerSafetyRestriction] public ComponentTypeHandle<T2> TypeHandle2;
            [NativeDisableContainerSafetyRestriction] public NativeArray<Entity> arrayEntities;
            [NativeDisableContainerSafetyRestriction] public NativeArray<T1> arrayType1;
            [NativeDisableContainerSafetyRestriction] public NativeArray<T2> arrayType2;
            public int entityIndexInQuery;

            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            {
                var e1 = arrayEntities.IsCreated;
                var t1 = arrayType1.IsCreated;
                var t2 = arrayType2.IsCreated;

                var entityArray = e1 ? chunk.GetNativeArray(entityType) : new NativeArray<Entity>();
                var componentArray1 = t1 ? chunk.GetNativeArray(ref TypeHandle1) : new NativeArray<T1>();
                var componentArray2 = t2 ? chunk.GetNativeArray(ref TypeHandle2) : new NativeArray<T2>();

                var enumerator = new ChunkEntityEnumerator(useEnabledMask, chunkEnabledMask, chunk.Count);
                while (enumerator.NextEntityIndex(out var i))
                {
                    if (e1) arrayEntities[entityIndexInQuery] = entityArray[i];
                    if (t1) arrayType1[entityIndexInQuery] = componentArray1[i];
                    if (t2) arrayType2[entityIndexInQuery] = componentArray2[i];
                    ++entityIndexInQuery;
                }
            }
        }
        [BurstCompile]
        public partial struct GetEntityComponentArraysAsyncParallelJob<T1, T2> : IJobChunk where T1 : unmanaged, IComponentData where T2 : unmanaged, IComponentData
        {
            [ReadOnly] public NativeArray<int> ChunkBaseEntityIndices;
            [NativeDisableContainerSafetyRestriction] public EntityTypeHandle entityType;
            [NativeDisableContainerSafetyRestriction] public ComponentTypeHandle<T1> TypeHandle1;
            [NativeDisableContainerSafetyRestriction] public ComponentTypeHandle<T2> TypeHandle2;
            [NativeDisableContainerSafetyRestriction] public NativeArray<Entity> arrayEntities;
            [NativeDisableContainerSafetyRestriction] public NativeArray<T1> arrayType1;
            [NativeDisableContainerSafetyRestriction] public NativeArray<T2> arrayType2;
            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            {
                var e1 = arrayEntities.IsCreated;
                var t1 = arrayType1.IsCreated;
                var t2 = arrayType2.IsCreated;

                var entityArray = e1 ? chunk.GetNativeArray(entityType) : new NativeArray<Entity>();
                var componentArray1 = t1 ? chunk.GetNativeArray(ref TypeHandle1) : new NativeArray<T1>();
                var componentArray2 = t2 ? chunk.GetNativeArray(ref TypeHandle2) : new NativeArray<T2>();

                int baseEntityIndex = ChunkBaseEntityIndices[unfilteredChunkIndex];
                int validEntitiesInChunk = 0;

                var enumerator = new ChunkEntityEnumerator(useEnabledMask, chunkEnabledMask, chunk.Count);
                while (enumerator.NextEntityIndex(out var i))
                {
                    int entityIndexInQuery = baseEntityIndex + validEntitiesInChunk;

                    if (e1) arrayEntities[entityIndexInQuery] = entityArray[i];
                    if (t1) arrayType1[entityIndexInQuery] = componentArray1[i];
                    if (t2) arrayType2[entityIndexInQuery] = componentArray2[i];

                    ++validEntitiesInChunk;
                }
            }
        }
        //////////////////////////////////////////////////////////////////////////////////    
    }
}