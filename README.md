﻿# MidnightCow
Some helpers and stuff for Unity and Unity's ECS/DOTS ecosystem.

**Package Import**

 - Go to Window menu, click Package Manager
 - Look for the little + icon on the top-left of the Package Manager window 
 - Click on 'Add package from git URL'
 
![enter image description here](https://gitlab.com/MidnightCow/MidnightCow/-/raw/main/help_img_addurl.png)

 - Copy-paste the following into the box:
 
https://gitlab.com/MidnightCow/MidnightCow.git?path=/com.midnightcow.core

- Then hit 'Add' and you're done!

The package should now be visible in Unity Package Manager ( UPM ):

![enter image description here](https://gitlab.com/MidnightCow/MidnightCow/-/raw/main/help_img_showpackage.png)

And when you want, you can update the package by hitting the Update button here:

![enter image description here](https://gitlab.com/MidnightCow/MidnightCow/-/raw/main/help_img_updatepackage.png)

Enjoy!

